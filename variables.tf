variable "region" {
  type        = string
  description = "AWS region"
}

variable "namespace" {
  type        = string
  description = "namespace"
}

variable "stage" {
  type        = string
  description = "stage"
}

variable "name" {
  type        = string
  description = "stage"
}

variable "image_names" {
  type        = list(any)
  description = "a list of image names will be deployed to ECR"
}

variable "docker_files" {
  type        = list(any)
  description = "a list of paths to dockerfile"
}