# TODO: Add variable for profile

# Retrieve an authentication token and authenticate your Docker client to your registry.
aws ecr get-login-password --region $REGION | docker login --username AWS --password-stdin $HOST
# Build your Docker image using the following command. 
docker build -t $IMAGE_NAME $DOCKER_FILE

# After the build completes, tag your image so you can push the image to this repository
docker tag $IMAGE_NAME:$IMAGE_TAG $HOST/$IMAGE_NAME:$IMAGE_TAG

# Run the following command to push this image to your newly created AWS repository:
docker push $HOST/$IMAGE_NAME:$IMAGE_TAG


